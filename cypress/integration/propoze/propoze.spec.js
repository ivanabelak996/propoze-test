describe('example to-do app', () => {
    before(() => {

        cy.visit('https://propoze.app/login')
    })

    it('login', () =>{
        cy.get('input.input.input--text[type=email]')
            .type('filipa.mikic@gmail.com')
        cy.get('input.input.input--text[type=password]')
            .type('filipa123')
        cy.get('button.btn.btn--med.btn--primary[value=Submit]').click()
    })
    it('checkLogin', () => {
        cy.contains('Recommended logo size for best preview in proposals is 200x200px in .jpg, .png or .svg format.')
    })

    it('onboarding', () => {
        cy.get('.input.input--text[name=name]')
            .type('Moja tvrtka 4')

        const logo = 'mojatvrtka.jpg'
        cy.get('input[type=file]').attachFile(logo)
        cy.get(".input.input--text[name='address.addressOne']")
            .type('Ulica Lipa 8')
        cy.get(".input.input--text[name='address.addressTwo']")
            .type('Ulica Vrba 8')
        cy.get('.css-1hwfws3').click()
        cy.get('#react-select-2-option-53').click()
        cy.get(".input.input--text[name='address.state']")
            .type('Osjecko-baranjska')
        cy.get(".input.input--text[name='address.city']")
            .type('Osijek')
        cy.get(".input.input--text[name='address.postalCode']")
            .type('31000')
        cy.get(".input.input--text[name='tax.vatId']")
            .type('vatid1')
        cy.get('.input.input--w-micro.s-right--sml')
            .type('1')
        cy.get('.btn.btn--med.btn--primary').contains('Save').click()
    })

    it('onboardingCheck', () => {
        cy.contains('My proposals')
    })
})